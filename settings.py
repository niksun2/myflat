import os

BASE_DIR = os.path.dirname(__file__)
CACHE_DIR = os.path.join(BASE_DIR, "cache")
RESOURCES_DIR = os.path.join(BASE_DIR, "resources")
SOUND_FILE = os.path.join(RESOURCES_DIR, "sound.wav")
BLACKLIST_USERNAMES_FILE = os.path.join(RESOURCES_DIR, "black_list_names.txt")
BLACKLIST_PHONES_FILE = os.path.join(RESOURCES_DIR, "black_list_phones.txt")

DEFAULT_MIN_PRICE = 200
DEFAULT_MAX_PRICE = 300
DEFAULT_LINK_COUNT = 10
LOOP_TIMEOUT_SECONDS = 60

USER_AGENT = r"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 " \
             r"(KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36"
REQUEST_TIMEOUT_SECONDS = 10


def initialize():
    os.makedirs(CACHE_DIR, exist_ok=True)
