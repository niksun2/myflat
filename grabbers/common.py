import os
import abc
from typing import List, Tuple, Generator, Union

import requests
from requests_html import HTMLSession, HTMLResponse

import settings


class BaseGrabber(abc.ABC):
    def __init__(self, min_price: int, max_price: int):
        self._min_price = min_price
        self._max_price = max_price
        self._cache = CacheHandler(os.path.join(settings.CACHE_DIR, self.get_source_name()))

    @staticmethod
    @abc.abstractmethod
    def get_source_name() -> str:
        pass

    @abc.abstractmethod
    def get_links(self) -> Generator[str, None, None]:
        pass

    def add_to_cache(self, link: str):
        self._cache.add(link)
        self._cache.flush()

    @staticmethod
    def _get(url: str, params: List[Tuple[str, str]] = None, is_html: bool = False) \
            -> Union[requests.Response, HTMLResponse]:
        while True:
            try:
                if is_html:
                    return HTMLSession().get(url, params=params, timeout=settings.REQUEST_TIMEOUT_SECONDS,
                                             headers={"User-Agent": settings.USER_AGENT})
                else:
                    return requests.get(url, params=params, timeout=settings.REQUEST_TIMEOUT_SECONDS)
            except requests.exceptions.Timeout:
                print(f"Timeout on {url}")


class CacheHandler:
    def __init__(self, filename: str):
        self._cache = set()
        self._filename = filename
        if os.path.exists(self._filename):
            self._initialize_cache()

    def _initialize_cache(self):
        with open(self._filename, "rt") as f:
            self._cache = {
                line.strip() for line in f.read().split('\n') if len(line) > 1
            }

    def check(self, flat_id: str) -> bool:
        return flat_id in self._cache

    def add(self, flat_id: str):
        self._cache.add(flat_id)

    def flush(self):
        with open(self._filename, "wt") as f:
            f.write("\n".join(self._cache))

    def __contains__(self, flat_id: str) -> bool:
        return flat_id in self._cache
