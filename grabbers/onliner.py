import os
import random
from typing import Generator, Set, Tuple

import settings
from grabbers.common import BaseGrabber


class OnlinerGrabber(BaseGrabber):
    REGIONS = (
        ("53.88542", "27.39403", "53.97624", "27.52427"),   # frunzenskiy
        ("53.90899", "27.48907", "53.97618", "27.61932"),   # north
        ("53.92546", "27.65844", "53.96979", "27.74437"),   # uruchie
        ("53.90077", "27.55056", "53.96798", "27.68080"),   # zel. lug
    )

    HARDCODED_QUERY_PARAMS = [
        ("rent_type[]", "1_room"),
        ("rent_type[]", "2_rooms"),
        ("currency", "usd"),
        ("only_owner", "true"),
        ("page", "1"),
    ]

    def __init__(self, min_price: int, max_price: int):
        self._bad_phones = self._read_black_list(settings.BLACKLIST_PHONES_FILE)
        self._bad_names = self._read_black_list(settings.BLACKLIST_USERNAMES_FILE)
        super().__init__(min_price, max_price)

    @staticmethod
    def _read_black_list(path: str) -> Set[str]:
        if os.path.exists(path):
            with open(path) as f:
                return {line.strip() for line in f.read().split('\n')}
        return set()

    def _send_request(self, region: Tuple[str, str, str, str]):
        url = r"https://ak.api.onliner.by/search/apartments"
        params = self.HARDCODED_QUERY_PARAMS + [
            ("price[min]", str(self._min_price)),
            ("price[max]", str(self._max_price)),
            ("bounds[lb][lat]", region[0]),
            ("bounds[lb][long]", region[1]),
            ("bounds[rt][lat]", region[2]),
            ("bounds[rt][long]", region[3]),
            ("v", str(random.random())),
        ]
        r = self._get(url, params)
        return r.json()

    @classmethod
    def _extract_user_info(cls, url):
        r = cls._get(url, is_html=True)
        phones_selector = '#apartment-phones > ul > li.apartment-info__item.apartment-info__item_secondary > a'
        name_selector = "#container > div > div.l-gradient-wrapper > div > div > div.arenda-apartment >" \
                        " div.apartment-info > div:nth-child(1) > div > " \
                        "div.apartment-info__cell.apartment-info__cell_33.apartment-info__cell_right >" \
                        " div:nth-child(3) > a"
        phones = [a.attrs['href'][4:] for a in r.html.find(phones_selector)]
        name = r.html.find(name_selector)[0].text
        return name, phones

    def _user_is_not_realtor(self, url):
        name, phones = self._extract_user_info(url)
        if name in self._bad_names or len(name) <= 1 or name.startswith("+375"):
            return False
        for phone in phones:
            if phone in self._bad_phones:
                return False
        return True

    def get_links(self) -> Generator[str, None, None]:
        for region in self.REGIONS:
            json_data = self._send_request(region)
            for apartment in json_data['apartments']:
                link = apartment['url']
                if link not in self._cache and self._user_is_not_realtor(link):
                    yield link

    @staticmethod
    def get_source_name() -> str:
        return "onliner"
