import re
from typing import Generator

from grabbers.common import BaseGrabber


class KvartirantGrabber(BaseGrabber):
    DISTRICTS = ("50", "60", "70", "80", "90")

    HARDCODED_QUERY_PARAMS = [
        ("tx_uedbadsboard_pi1[search][q]", ""),
        ("tx_uedbadsboard_pi1[search][rooms][1]", "1"),
        ("tx_uedbadsboard_pi1[search][rooms][2]", "2"),
        ("tx_uedbadsboard_pi1[search][currency]", "840"),
        ("tx_uedbadsboard_pi1[search][date]", "86400"),
        ("tx_uedbadsboard_pi1[search][agency_id]", ""),
        ("tx_uedbadsboard_pi1[search][owner]", "on"),
        ("tx_uedbadsboard_pi1[search][remember]", "1"),
    ]

    def _send_request(self, district):
        url = r"https://www.kvartirant.by/ads/flats/type/rent"
        params = self.HARDCODED_QUERY_PARAMS + [
            ("tx_uedbadsboard_pi1[search][district]", district),
            ("tx_uedbadsboard_pi1[search][price][from]", str(self._min_price)),
            ("tx_uedbadsboard_pi1[search][price][to]", str(self._max_price)),
        ]
        r = self._get(url, params=params)
        return re.findall(r"https://www\.kvartirant\.by/ads/flats/rent/\d+/", r.content.decode())

    def get_links(self) -> Generator[str, None, None]:
        for district in self.DISTRICTS:
            new_flats = self._send_request(district)
            for link in new_flats:
                if link not in self._cache:
                    yield link

    @staticmethod
    def get_source_name() -> str:
        return "kvartirant"
