from typing import Generator, Tuple, List, Union
from requests_html import Element

from grabbers.common import BaseGrabber


class RealtGrabber(BaseGrabber):
    HARDCODED_QUERY_PARAMS = [
            ("tx_uedbflatrent_pi2[DATA][state_region_id][e]", ""),
            ("tx_uedbflatrent_pi2[DATA][state_district_id][e]", ""),
            ("tx_uedbflatrent_pi2[DATA][town_id][e]", ""),
            ("tx_uedbflatrent_pi2[DATA][town_name][like][0]", ""),
            ("tx_uedbflatrent_pi2[DATA][town_name][like][1]", ""),
            ("tx_uedbflatrent_pi2[DATA][town_name][like][2]", ""),
            ("tx_uedbflatrent_pi2[DATA][town_name][like][3]", ""),
            ("tx_uedbflatrent_pi2[DATA][town_name][like][4]", ""),
            ("tx_uedbflatrent_pi2[DATA][street_name][like][0]", ""),
            ("tx_uedbflatrent_pi2[DATA][house_number][range][0]", ""),
            ("tx_uedbflatrent_pi2[DATA][street_name][like][1]", ""),
            ("tx_uedbflatrent_pi2[DATA][house_number][range][1]", ""),
            ("tx_uedbflatrent_pi2[DATA][street_name][like][2]", ""),
            ("tx_uedbflatrent_pi2[DATA][house_number][range][2]", ""),
            ("tx_uedbflatrent_pi2[DATA][street_name][like][3]", ""),
            ("tx_uedbflatrent_pi2[DATA][house_number][range][3]", ""),
            ("tx_uedbflatrent_pi2[DATA][street_name][like][4]", ""),
            ("tx_uedbflatrent_pi2[DATA][house_number][range][4]", ""),
            ("tx_uedbflatrent_pi2[DATA][town_subdistrict_id][e][]", "#50"),
            ("tx_uedbflatrent_pi2[DATA][town_subdistrict_id][e][]", "#60"),
            ("tx_uedbflatrent_pi2[DATA][town_subdistrict_id][e][]", "#70"),
            ("tx_uedbflatrent_pi2[DATA][town_subdistrict_id][e][]", "#80"),
            ("tx_uedbflatrent_pi2[DATA][town_subdistrict_id][e][]", "#90"),
            ("tx_uedbflatrent_pi2[DATA][rooms][e][1]", "1"),
            ("tx_uedbflatrent_pi2[DATA][rooms][e][2]", "2"),
            ("tx_uedbflatrent_pi2[DATA][appliances][bs][]", "1"),
            ("tx_uedbflatrent_pi2[DATA][storeys][ge]", ""),
            ("tx_uedbflatrent_pi2[DATA][storeys][le]", ""),
            ("tx_uedbflatrent_pi2[DATA][storey][ge]", ""),
            ("tx_uedbflatrent_pi2[DATA][storey][le]", ""),
            ("tx_uedbflatrent_pi2[DATA][term_of_lease][e]", ""),
            ("tx_uedbflatrent_pi2[DATA][prepayment][e]", ""),
            ("tx_uedbflatrent_pi2[DATA][terms][e]", ""),
            ("tx_uedbflatrent_pi2[DATA][x_days_old][e]", "3"),
            ("tx_uedbflatrent_pi2[DATA][agency_id][e]", ""),
            ("tx_uedbflatrent_pi2[DATA][x_only_private][e]", "1"),
            ("tx_uedbflatrent_pi2[rec_per_page]", "50"),
            ("tx_uedbflatrent_pi2[sort_by][0]", "date_revision"),
            ("tx_uedbflatrent_pi2[asc_desc][0]", "0"),
            ("tx_uedbflatrent_pi2[DATA][x_count_pictures][ge]", "1"),
            ("tx_uedbflatrent_pi2[sort_by][1]", ""),
            ("tx_uedbflatrent_pi2[asc_desc][1]", "0"),
            ("c1", ""),
    ]

    @classmethod
    def _get_secret_hash(cls) -> str:
        r = cls._get("https://realt.by/rent/flat-for-long/search/", is_html=True)
        return r.html.find('#secret-hash', first=True).attrs['value']

    def _get_query_params(self) -> List[Tuple[str, str]]:
        secret_hash = self._get_secret_hash()
        return self.HARDCODED_QUERY_PARAMS + [
            ("hash", secret_hash),
            ("tx_uedbflatrent_pi2[DATA][price][ge]", self._min_price),
            ("tx_uedbflatrent_pi2[DATA][price][le]", self._max_price),
        ]

    def _get_elements(self) -> Union[List[Element]]:
        url = r"https://realt.by/rent/flat-for-long/"
        params = self._get_query_params()
        r = self._get(url, params=params, is_html=True)
        return r.html.find('div.desc > a')

    def get_links(self) -> Generator[str, None, None]:
        new_flats_a = self._get_elements()
        for a in new_flats_a:
            link = a.attrs['href']
            if link is not None and link not in self._cache:
                yield link

    @staticmethod
    def get_source_name() -> str:
        return "realt"
