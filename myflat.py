import argparse
import time
import webbrowser
import winsound
import enum
from typing import Dict, Any, List

from grabbers import *
import settings
from grabbers.common import BaseGrabber


class AppMode(enum.Enum):
    print = "print"
    open = "open"

    def __str__(self):
        return self.value


def handle_arguments() -> Dict[str, Any]:
    parser = argparse.ArgumentParser(description="Utility for searching new rent flat")
    parser.add_argument("-m", "--mode", type=AppMode, choices=list(AppMode), default="print",
                        help="Mode 'open' opens URLs in default browser. Mode 'print' just lists them in STDOUT")
    parser.add_argument("--min-price", type=int, default=settings.DEFAULT_MIN_PRICE,
                        help=f"Set minimum price of suggested flats. Default: {settings.DEFAULT_MIN_PRICE}")
    parser.add_argument("--max-price", type=int, default=settings.DEFAULT_MAX_PRICE,
                        help=f"Set maximum price of suggested flats. Default: {settings.DEFAULT_MAX_PRICE}")
    parser.add_argument("-c", "--count", type=int, default=settings.DEFAULT_LINK_COUNT,
                        help=f"Max count of opened/printed links at a time. Default: {settings.DEFAULT_LINK_COUNT}")
    parser.add_argument("-d", "--daemon", action="store_true",
                        help="If the flag is set app will search fresh ads infinitely. "
                             "Otherwise, the app will find new flats only one time")
    args = parser.parse_args()
    return vars(args)


def init_grabbers(min_price: int, max_price: int) -> List[BaseGrabber]:
    return [
        OnlinerGrabber(min_price, max_price),
        RealtGrabber(min_price, max_price),
        KvartirantGrabber(min_price, max_price),
    ]


def open_in_browser(link: str):
    webbrowser.open_new_tab(link)


def handle_link(link: str, mode: AppMode):
    if mode is AppMode.print:
        print(link)
    elif mode is AppMode.open:
        open_in_browser(link)
    else:
        raise AssertionError(f"Undefined app mode: {mode}")


def grab_iteration(grabbers: List[BaseGrabber], mode: AppMode, max_count: int) -> int:
    count = 0
    for grabber in grabbers:
        for link in grabber.get_links():
            handle_link(link, mode)
            grabber.add_to_cache(link)
            count += 1
            if count >= max_count:
                return count
    return count


def main():
    settings.initialize()
    args = handle_arguments()
    grabbers = init_grabbers(args["min_price"], args["max_price"])
    print("Processing...")
    while True:
        found_links_count = grab_iteration(grabbers, args["mode"], args["count"])
        if args["daemon"] is False:
            break
        if found_links_count > 0:
            winsound.PlaySound(settings.SOUND_FILE, winsound.SND_FILENAME)
        time.sleep(settings.LOOP_TIMEOUT_SECONDS)


if __name__ == '__main__':
    main()
