OBJECTIVES
	The app was developed by myself for convenient searching new longtime rent flats.
	There were two main goals:
		- be the first caller to the flat owner (it's important in my local rent market)
		- avoid manual searching into several rent-platforms (three ones in my case)
	All technical solutions in the project depend from next conditions:
		- it's a home project for personal purposes 
		- code simplicity more important then app flexibility
		- target platform - windows (my home PC)
	
DESCRIPTION
	App has a command-line interface built by build-in package 'argparse'.
	The app collects links on new ads from several websites and provides them to users in that form which was chosen as mode.
	There are two modes: mode 'print' just prints links into STDOUT, mode 'open' opens links in the default browser.
	Requests to websites are managed by a third-part package 'requests_html'.
	The format of requests and responses was explored by myself with help of Chrome's 'developers tools' because rent platforms don't have a public API.
	Visited links save into cache files for avoiding double open.
	File settings.py contains several configuration entries (it looks like Django setting.py).

USAGE
	The entry script is myflat.py.	
	Command-line arguments:
		--mode - There are two modes as described upper: open and print. Print is the default.
		--min-price - Set minimum price of searched flats.
		--max-price - Set maximum price of searched flats.
		--count - Maximum count of opened/printed links at a time.
		--daemon - If the flag is set app will search fresh ads infinitely. Otherwise, the app will find new flats only one time.
	All arguments are optional.
